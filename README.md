# README #

This README of generic order backend package

## What is this repository for? ##

Package covers sample package functionality including database, routes etc.

## How do I get set up? ##

### 1. composer.json ###

Link repository of package:

```
"repositories": [
	{
		"type": "url",
		"url": "https://bitbucket.org/in-solutions/laravel-package"
	}
]
```

and then run

`composer require ins/ecommerce:master-dev`

### 2. Publish sources ###

`php artisan vendor:publish`

- publishes db migrations into /database/sql/ins/package
- publishes listeners/subscribers into /app/Listeners

### 3. Register package ServiceProvider ###

in file _app/config.php_ extend array by line:

```
"providers" => [
	
	...
		
	INS\Package\ServiceProvider::class,		
	
]
```

### 4. Register subscriber ###

in file _app/Providers/EventServiceProvider.php_ extend array by line:

```
protected $subscribe = [
	
	...
	
	'App\Listeners\PackageSubscriber',
	
];
```

## Modules interface ##

Packages can not be dependent on any project-specific application. Only allowed dependencies are allowed to other laravel packages.

Project specific actions after some functionality (*Events*) covered by package can be defined in *Listeners* published into _app/Listeners_ folder when installing composer package.

Package Listener: _app/Listeners/PackageSubscriber.php_

### Subscribed events: ###

- *onEntityCreated*: when new order is created, $event->order = Order model

## Who do I talk to? ##

Jakub Lajmon <jakub@lajmon.name>
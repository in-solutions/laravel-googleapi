<?php

Route::group(['prefix' => 'googleapi'], function () {
	
	Route::get('getAccess', 'Insolutions\GoogleApi\Controller@getAccess');
	Route::get('callback', 'Insolutions\GoogleApi\Controller@callback')->name('googleapi-callback');

	Route::get('upload_file/{file_id}', 'Insolutions\GoogleApi\Controller@uploadFile');
});
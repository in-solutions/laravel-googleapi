<?php

namespace Insolutions\GoogleApi;

use Illuminate\Database\Eloquent\Model;

use App\User;

class UserGoogle extends Model
{
    protected $table = 't_user_google';
    protected $primaryKey = 'user_id';
    protected $fillable = ['user_id'];

    public static function findByUser(User $user) {
    	return self::firstOrCreate(['user_id' => $user->id]);
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function setAccessToken($accessToken) {
        $this->access_token = json_encode($accessToken);
    }

    public function getAccessToken() {
        return $this->access_token ? json_decode($this->access_token) : null;
    }

    public function getStateHash() {
        return sha1($this->user->created_at) . $this->user_id;
    }

    public static function findByStateHash($hash) {
        $sha1_length = 40;

        // parse user_id and check hash
        $user_id = substr($hash, $sha1_length); // get what is after sha1 hash length

        // load UserGoogle by user_id
        $gu = UserGoogle::find($user_id);
        if ($gu && ($hash == $gu->getStateHash())) {
            // if user was found and stateHashes are same -> correct user is loaded, return it
            return $gu;
        } else {
            return null;
        }
    }
}

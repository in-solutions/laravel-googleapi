<?php

namespace Insolutions\GoogleApi;
 
use Illuminate\Http\Request;

use Insolutions\Auth\MiddlewareOnlyAuth as MiddlewareOnlyAuth;
use Auth, App\User;
use Insolutions\Files\File as FileEntry;

class Controller extends \App\Http\Controllers\Controller
{
    
	public function __construct() {
		// for all actions, there must be logged user
		// callback will not contain header, as its called by remote server
		$this->middleware(MiddlewareOnlyAuth::class)->except('callback');
	}

	public function uploadFile($file_id) {
		$file = FileEntry::findOrFail($file_id);

		$gu = UserGoogle::findByUser(Auth::user());

		try {
			$gid = Google::uploadFile($gu, $file);
		} catch (Exception $e) {
			Log::error($e->getMessage());
			abort(500, 'Upload failed');
		}
	
		return response()->json([
			'google_file_id' => $gid,
			'google_file_url' => 'http://drive.google.com/open?id=' . $gid
		]);		
	}

	public function getAccess(Request $r) {
		$gu = UserGoogle::findByUser(Auth::user());

		$client = Google::client($gu);

		$stateData = ['hash' => $gu->getStateHash()];

		if ($r->redirect_uri) {
			$stateData['final_url'] = $r->redirect_uri;
		}

		$client->setState(json_encode($stateData));

		$authUrl = $client->createAuthUrl();

		return response()->json([
			'auth_url' => $authUrl
		]);
	}

	public function callback(Request $r) {
		if ($r->code) {
			$client = Google::client();
			$accessToken = $client->fetchAccessTokenWithAuthCode($r->code);

			if (isset($accessToken['error'])) {
				return response()->json(['accessToken' => $accessToken], 412); // HTTP 412 Precondition Failed
			}

			$stateData = json_decode($r->state);

			$gu = UserGoogle::findByStateHash($stateData->hash);
			if (!$gu) {
				throw new \InvalidStateException('No user specified in google service callback!');
			}
			$gu->setAccessToken($accessToken);
			$gu->save();

			// access token is stored
			return redirect(isset($stateData->final_url) ? $stateData->final_url : '/');

		} else {		
			return response('No code received', 412); // HTTP 412 Precondition Failed
		}
	}
}
<?php 

namespace Insolutions\GoogleApi;

use Illuminate\Support\Facades\Storage;

use GoogleUser;
use Insolutions\Files\File as FileEntry;

class Google
{
    public static function client(UserGoogle $gu = null)
    {
        $client = new \Google_Client();
        $client->setClientId(config('google.client_id'));
        $client->setClientSecret(config('google.client_secret'));
        $client->setRedirectUri(route('googleapi-callback'));
        $client->setScopes(config('google.scopes'));
        $client->setApprovalPrompt(config('google.approval_prompt'));
        $client->setAccessType(config('google.access_type'));

        if ($gu && $gu->access_token) {
            $client->setAccessToken($gu->access_token);

            // Refresh the token if it's expired.
            self::refreshToken($client, $gu);
        }

        return $client;
    }

    private static function refreshToken($client, $gu) {
        if ($client->isAccessTokenExpired()) {
            $refresh_token = $client->getRefreshToken();
            if ($refresh_token) {
                $client->fetchAccessTokenWithRefreshToken($refresh_token);
                // store refreshed accessToken
                $gu->setAccessToken($client->getAccessToken());
                $gu->save();
            }
        }
    }

    public static function uploadFile(UserGoogle $gu, FileEntry $fileEntry, $folder_id = null) {
        $service = new \Google_Service_Drive(self::client($gu));

        $content = Storage::get($fileEntry->getFilePath());

        // Now lets try and send the metadata as well using multipart!
        $gfileMetaData = [];
        if ($folder_id) {
            $gfileMetaData['parents'] = array($folder_id);
        }

        $file = new \Google_Service_Drive_DriveFile($gfileMetaData);
        $file->setName($fileEntry->title);
        $result = $service->files->create(
            $file,
                array(
                    'data' => $content,
                    'mimeType' => $fileEntry->mime_type,
                    'uploadType' => 'multipart'
                )
        );

        return $result->id;
    }

    public static function readSheet(UserGoogle $gu, $gsheet_id, $sheet_name, $range) {
        $grange = "{$sheet_name}!{$range}";
        
        $service = new \Google_Service_Sheets(self::client($gu));

        // reading data             
        $response = $service->spreadsheets_values->get($gsheet_id, $grange);
        $values = $response->getValues();

        return $values;
    }

    public static function updateSheet(UserGoogle $gu, $gsheet_id, $sheet_name, $range, $data, $format = null) {
        if (!$format) {
            $format = 'USER_ENTERED'; // if not given
        }
        $grange = "{$sheet_name}!{$range}";

        $service = new \Google_Service_Sheets(self::client($gu));

        // writing data     
        $body = new \Google_Service_Sheets_ValueRange(array(
          'values' => $data
        ));
        $params = array(
          'valueInputOption' => $format
        );
        $result = $service->spreadsheets_values->update($gsheet_id, $grange,
            $body, $params);
    }

}